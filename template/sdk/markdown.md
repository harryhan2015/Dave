    
**简要描述：** 

- 员工用户登录接口

**请求URL：** 
- userlogin/
  
**请求方式：**
- POST 

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |是  |string |用户名   |
|userpass |是  |string | 密码    |
|name     |否  |string | 加密后的密码    |

 **返回示例**

``` 
  {
    "error_code": 0,
    "data": {
      "uid": "1",
      "username": "12154545",
      "name": "吴系挂",
      "groupid": 2 ,
      "reg_time": "1436864169",
      "last_login_time": "0",
    }
  }
```

 **返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|User_info表所有字段（密码除外） |int   |字段说明：Field6:姓名Field7:证件号Field8:手机号码Field9:会员等级：0：普通会员1：VIP会员2：SVIP会员Field10:会员状态  |
|Structid |int   |机构id  |
|Structname |int   |机构名称  |
|Menutype |int   |菜单类型0医生1护士2治疗师3管理员  |

 **备注** 

- 更多返回错误代码请看首页的错误代码描述


